﻿using Leap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointContact : MonoBehaviour
{
    PointManager pointManager;
    // Use this for initialization
    void Start()
    {
        GameObject text = GameObject.Find("Text");
        pointManager = (PointManager)text.GetComponent("PointManager");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (!IsHand(collision))
        {
            pointManager.Point++;
            Destroy(collision.gameObject);
        }
    }

    private bool IsHand(Collision collision)
    {
        if (collision.transform.parent && collision.transform.parent.parent && collision.transform.parent.parent.GetComponent<HandModel>())
            return true;
        else
            return false;
    }
}
