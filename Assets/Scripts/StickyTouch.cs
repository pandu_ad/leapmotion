﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class StickyTouch : MonoBehaviour {
	private int countFinger;
	Controller controller;
	HandController handController;

	// Use this for initialization
	void Start () {
		countFinger = 0;
		controller = new Controller ();
		GameObject box = GameObject.Find ("ControllerSandBoxdBox");
		handController = (HandController)box.GetComponent("HandController");
	}

	// Update is called once per frame
	void Update () {
		if (countFinger >= 3) {
			gameObject.transform.position = handController.transform.TransformPoint(
				controller.Frame().Hands[0].PalmPosition.ToUnityScaled()
			);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		FingerModel finger = collision.gameObject.GetComponentInParent<FingerModel>();
		if (finger.fingerType == Finger.FingerType.TYPE_THUMB ||
			finger.fingerType == Finger.FingerType.TYPE_MIDDLE ||
			finger.fingerType == Finger.FingerType.TYPE_PINKY) {
			countFinger++;
		}
	}

	void OnCollisionExit(Collision collision){
		FingerModel finger = collision.gameObject.GetComponentInParent<FingerModel>();
		if (finger.fingerType == Finger.FingerType.TYPE_THUMB ||
			finger.fingerType == Finger.FingerType.TYPE_MIDDLE ||
			finger.fingerType == Finger.FingerType.TYPE_PINKY) {
			countFinger--;
		}
	}
}
