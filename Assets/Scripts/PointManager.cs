﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointManager : MonoBehaviour {
    public int Point;
    Text textPoint;

    // Use this for initialization
    void Start ()
    {
        Point = 0;
        textPoint = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        textPoint.text = Point.ToString();
    }
}
